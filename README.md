# Folder Viewer (Google Drive Simulator)

This is a folder viewer similar in style to google drive which retrieves data from a json file.

## The file data.
When running the app with `npm run serve:app+data`

It will serve the data locally, load in db.json from http://localhost:3000/files