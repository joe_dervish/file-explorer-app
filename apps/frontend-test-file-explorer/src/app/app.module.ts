import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileExplorerHeaderComponent } from './modules/landing/pages/landing/components/file-explorer/file-explorer-header/file-explorer-header.component';

@NgModule({
  declarations: [AppComponent, FileExplorerHeaderComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, RouterModule, BrowserAnimationsModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
