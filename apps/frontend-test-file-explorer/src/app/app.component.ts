import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { faderAnimation } from './modules/landing/pages/landing/animations/fader';

@Component({
  selector: 'holition-frontend-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    faderAnimation
  ]
})
export class AppComponent {
  title = 'frontend-file-reader-test';

  public prepareRoute(outlet: RouterOutlet) {
    return outlet?.activatedRouteData?.['animation'];
  }
}

