export interface FileDTO {
    path: string
    sizeInBytes: number
}