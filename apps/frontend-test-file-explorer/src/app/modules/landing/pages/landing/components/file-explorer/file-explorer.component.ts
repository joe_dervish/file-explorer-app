import { Component, OnInit } from '@angular/core';
import { FileExplorerService } from '../../services/file-explorer-list.service';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { FileDTO } from '../../models/FileDTO';
import { FileHierarchy } from '../../models/FileHierarchy';
import { DriveFolder } from '../../models/DriveFolder';
import { DriveFile } from '../../models/DriveFile';

@Component({
  selector: 'holition-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss'],
})
export class FileExplorerComponent implements OnInit {
  files: any = [];
  rawPath = '';
  path = '';
  pathDirectories: any = [];
  lastPathSegment = '';
  fileExtension = '';
  dataLoaded = true;
  public fileHierarchy: FileHierarchy = {};
  public paths: string[] = [];
  public items: Array<DriveFile | DriveFolder> = [];

  constructor(public route: ActivatedRoute, public router: Router, public fileExplorersService : FileExplorerService) {
  }

  ngOnInit(): void {
    this.fileExplorersService.getJSON().subscribe(data => {
      this.files = data;
      this.createFileHierarchy(data); 
      this.route.url.subscribe((segments: UrlSegment[]) => {
        this.paths = segments.map((s) => s.path);
        this.setItems(this.paths);
      });
    });
  }

  private createFileHierarchy(data: Array<FileDTO>){
    const root: FileHierarchy = {}
    for(const file of data){
        const [...segments] = file.path.substr(1).split('/');
        const fileName = segments.pop() as string;
        let currentLevel = root;   
        while(segments.length >0){
           const folderName = segments.shift() as string;
           if(!currentLevel[folderName]){
            currentLevel[folderName] = {
              name: folderName,
              type: "folder",
              path: file.path,
              size: 0,
              children: {}
            };
           }        
           currentLevel = (currentLevel[folderName] as DriveFolder).children;
        }
        currentLevel[fileName] = {
          name: fileName,
          type: "file",
          path: file.path,
          extension: this.getFileType(fileName),
          size: file.sizeInBytes
        }
    }
    this.fileHierarchy = root;
  }

  public setItems(paths: string[]) {
    let currentLevel = this.fileHierarchy;
    for (const path of paths) {
      if (path in currentLevel) {
        if (currentLevel[path].type == 'folder') {
          currentLevel = (currentLevel[path] as DriveFolder).children;
        } else if (currentLevel[path].type == 'file') {
          // open the file and break out of the loop
        }
      }
    }

    this.items = [];
    for (const itemName in currentLevel) {
      this.items.push(currentLevel[itemName]);
    }
    this.dataLoaded = false;
  }

  public getFileName(fileName: string) {
    this.rawPath = fileName;
    this.path = this.rawPath.substring(1);
    this.pathDirectories = this.path.split("/");
    this.lastPathSegment = this.pathDirectories[this.pathDirectories.length - 1];
    return this.lastPathSegment;
  }

  public getFileType(fileName: string) {
    this.lastPathSegment = this.getFileName(fileName);
    this.fileExtension = this.lastPathSegment.replace(/^.*\./, '');
    return this.fileExtension;
  }
}
