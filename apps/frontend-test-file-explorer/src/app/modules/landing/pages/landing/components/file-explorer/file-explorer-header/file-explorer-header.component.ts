import { Component } from '@angular/core';

@Component({
  selector: 'holition-file-explorer-header',
  templateUrl: './file-explorer-header.component.html',
  styleUrls: ['./file-explorer-header.component.scss']
})
export class FileExplorerHeaderComponent {
  public title = 'File Explorer';
  public rootPath = '/drive';
}
