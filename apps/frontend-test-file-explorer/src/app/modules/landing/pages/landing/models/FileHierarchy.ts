import { DriveFile } from "./DriveFile";
import { DriveFolder } from "./DriveFolder";

export interface FileHierarchy {
    [key: string]: DriveFile | DriveFolder
}