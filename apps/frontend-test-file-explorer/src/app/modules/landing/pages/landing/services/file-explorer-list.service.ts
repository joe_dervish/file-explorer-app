import { HttpClient } from '@angular/common/http'; 
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FileDTO } from '../models/FileDTO';

@Injectable()
export class FileExplorerService {
   constructor(private http: HttpClient) {
    }

    public getJSON(): Observable<Array<FileDTO>> {
        return this.http.get<Array<FileDTO>>("http://localhost:3000/files");
    }
}