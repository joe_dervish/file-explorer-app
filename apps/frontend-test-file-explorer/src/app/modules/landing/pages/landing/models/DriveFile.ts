export interface DriveFile {
    name: string;
    type: string;
    path: string;
    extension: string;
    size: number;
}
