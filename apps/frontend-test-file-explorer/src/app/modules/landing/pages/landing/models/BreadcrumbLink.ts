export interface BreadcrumbLink {
    name: string;
    path: string;
}