import { Component } from '@angular/core';

@Component({
  selector: 'holition-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent {
  public title = 'File Explorer';
  public rootPath = '/drive';
}
