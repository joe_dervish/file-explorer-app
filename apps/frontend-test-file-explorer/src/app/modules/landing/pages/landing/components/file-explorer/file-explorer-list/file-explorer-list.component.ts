import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FileHierarchy } from '../../../models/FileHierarchy';
import { DriveFolder } from '../../../models/DriveFolder';
import { DriveFile } from '../../../models/DriveFile';

@Component({
  selector: 'holition-file-explorer-list',
  templateUrl: './file-explorer-list.component.html',
  styleUrls: ['./file-explorer-list.component.scss'],
})
export class FileExplorerListComponent {
  foldersPath = '../../../assets/folders';
  fileSize = 0;
  fileSizeText = '';
  rawPath = '';
  path = '';
  pathDirectories: any = [];
  lastPathSegment = '';
  fileExtension: any = '';
  srcImage: any = '';
  containsFiles = false;
  mediaItems: any;
  imgPaths: any = [];
  public showOverlay = false;

  @Input()
  public fileHierarchy: FileHierarchy = {};

  @Input()
  public items: Array<DriveFile | DriveFolder> = [];

  public get files() {
    return this.items.filter(i=>i.type==='file') as Array<DriveFile>
  }

  public get folders() {
    return this.items.filter(i=>i.type==='folder') as Array<DriveFolder>
  }

  constructor(
    public sanitizer: DomSanitizer
  ) {
    this.sanitizer = sanitizer;
  }

  public getFileName(fileName: string) {
    this.path = fileName;
    this.pathDirectories = this.path.split('/');
    this.lastPathSegment =
    this.pathDirectories[this.pathDirectories.length - 1];
    return this.lastPathSegment;
  }

  public getFileType(fileName: string) {
    this.lastPathSegment = this.getFileName(fileName);
    this.fileExtension = this.lastPathSegment.replace(/^.*\./, '');
    return this.fileExtension;
  }

  public getFileTypeImage(fileName: string) {
    this.fileExtension = this.getFileType(fileName);
    if (this.fileExtension == 'jpg' || this.fileExtension == 'png') {
      this.srcImage = '../../../assets/icons/jpeg.png';
    } else if (this.fileExtension == 'zip') {
      this.srcImage = '../../../assets/icons/zip.png';
    } else if (this.fileExtension == 'txt') {
      this.srcImage = '../../../assets/icons/plain.png';
    }
    return this.srcImage;
  }

  public convertFileSize(fileSizeValue: number) {
    if (fileSizeValue < 1000000) {
      this.fileSize = fileSizeValue / 1000;
      this.fileSize | 0;
      this.fileSizeText = this.fileSize + 'KB';
    } else if (fileSizeValue >= 1000000) {
      this.fileSize = fileSizeValue / 1000000;
      this.fileSize | 0;
      this.fileSizeText = this.fileSize + 'MB';
    }
    return this.fileSizeText;
  }

  public iframeSecureURL(filepath: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(filepath);
  }
}
