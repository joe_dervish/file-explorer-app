import { FileHierarchy } from "./FileHierarchy";

export interface DriveFolder {
    name: string;
    type: string;
    path: string;
    size: number;
    children: FileHierarchy;
}
