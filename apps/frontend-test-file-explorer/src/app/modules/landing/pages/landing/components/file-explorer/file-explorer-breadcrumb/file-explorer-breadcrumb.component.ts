import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { BreadcrumbLink } from '../../../models/BreadcrumbLink';
import { DriveFile } from '../../../models/DriveFile';
import { DriveFolder } from '../../../models/DriveFolder';

@Component({
  selector: 'holition-file-explorer-breadcrumb',
  templateUrl: './file-explorer-breadcrumb.component.html',
  styleUrls: ['./file-explorer-breadcrumb.component.scss']
})
export class FileExplorerBreadcrumbComponent implements OnInit {
  rootPath = '/drive';
  public breadcrumbLinks: Array<BreadcrumbLink> = [];

  @Input()
  public items: Array<DriveFile | DriveFolder> = [];
  public paths: Array<string> = [];
  
  constructor(
    public route: ActivatedRoute,
    public router: Router
  ) {
    
  }

  ngOnInit(): void {
    this.route.url.subscribe((segments: UrlSegment[]) => {
      this.paths = segments.map((s) => s.path);
      this.breadcrumbLinks = [];
      
      for(let i = 0; i < this.paths.length ; i++) {
        this.breadcrumbLinks.push({
          name: this.paths[i],
          path: this.rootPath + "/" + this.paths.slice(0,i+1).join("/")
        });
      }
    });
  }
}
