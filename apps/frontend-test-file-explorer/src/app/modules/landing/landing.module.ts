import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './pages/landing/landing.component';
import { FileExplorerService } from './pages/landing/services/file-explorer-list.service';
import { FileExplorerComponent } from '../landing/pages/landing/components/file-explorer/file-explorer.component';
import { FileExplorerListComponent } from '../landing/pages/landing/components/file-explorer/file-explorer-list/file-explorer-list.component';
import { RouterModule } from '@angular/router';
import { FileExplorerBreadcrumbComponent } from './pages/landing/components/file-explorer/file-explorer-breadcrumb/file-explorer-breadcrumb.component';

@NgModule({
  declarations: [LandingComponent, FileExplorerComponent, FileExplorerListComponent, FileExplorerBreadcrumbComponent],
  imports: [CommonModule, LandingRoutingModule, HttpClientModule, RouterModule],
  providers: [FileExplorerService],
  exports: [LandingComponent],
})
export class LandingModule {}
